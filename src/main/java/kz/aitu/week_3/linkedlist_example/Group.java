package kz.aitu.week_3.linkedlist_example;

import lombok.Data;

@Data
public class Group {
    private Block first;
    private Block last;

    public void addBlock(Block block) {
        if(first == null) {
            first = block;
            last = block;
        } else {
            last.setNextBlock(block);
            last = block;
        }
    }
}
